<?php
	class Url
	{
	    private static $url = null;
	    private static $baseUrl = null;

	    public static function getURL( $id )
	    {
		if( self::$url == null )
		// Verifica se a lista de URL já foi preenchida
		    self::getURLList();

		// Valida se existe o ID informado e retorna.
		if( isset( self::$url[ $id ] ) )
		    return self::$url[ $id ];

		// Caso não exista o ID, retorna nulo
		return null;
	    }

	    public static function getBase()
	    {
		if( self::$baseUrl != null )
		    return self::$baseUrl;

		global $_SERVER;
		$startUrl = strlen( $_SERVER["DOCUMENT_ROOT"] );
		$excludeUrl = substr( $_SERVER["SCRIPT_FILENAME"], $startUrl, -9 );
		if( $excludeUrl[0] == "/" )
		    self::$baseUrl = $excludeUrl; 
		else
		    self::$baseUrl = "/" . $excludeUrl;
		return self::$baseUrl;
	    }

	    private static function getURLList()
	    {
		global $_SERVER;

		// Primeiro traz todos as pastas abaixo do index.php
		$startUrl = strlen( $_SERVER["DOCUMENT_ROOT"] ) -1;
		$excludeUrl = substr( $_SERVER["SCRIPT_FILENAME"], $startUrl, -10 );

		// a variável$request possui toda a string da URL após o domínio.
		$request = $_SERVER['REQUEST_URI'];

		// Agora retira toda as pastas abaixo da pasta raiz
		$request = substr( $request, strlen( $excludeUrl ) );

		// Explode a URL para pegar retirar tudo após o ?
		$urlTmp = explode("?", $request);
		$request = $urlTmp[ 0 ];

		// Explo a URL para pegar cada uma das partes da URL
		$urlExplodida = explode("/", $request);

		$retorna = array();

		for($a = 0; $a <= count($urlExplodida); $a ++)
		{
		    if(isset($urlExplodida[$a]) AND $urlExplodida[$a] != "")
		    {
			array_push($retorna, $urlExplodida[$a]);
		    }
		}
		self::$url = $retorna;
	    }
	}
?>
	<!doctype html>
	<html lang="pt-br">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--[if IE]><link rel="shortcut icon" href="assets/img/favicon.png"><![endif]-->
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon.png" sizes="16x16">
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon-32.png" sizes="32x32">
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon-48.png" sizes="48x48">
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon-64.png" sizes="64x64">
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon-114.png" sizes="114x114">
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon-128.png" sizes="128x128">
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon-144.png" sizes="144x144">
		<link rel="icon" href="<?php echo URL::getBase(); ?>assets/img/favicon-512.png" sizes="512x512">
		<link rel="apple-touch-icon-precomposed" href="<?php echo URL::getBase(); ?>assets/img/apple-touch-icon-precomposed.png">


		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


		<title>Pedragon</title>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-103932828-20"></script>
		<script>
			window.dataLayer = window.dataLayer || [];

			function gtag() {
				dataLayer.push(arguments);
			}
			gtag('js', new Date());

			gtag('config', 'UA-103932828-20');
		</script>
		
		<?php include 'assets/css/style.php' ?>

	</head>

	<body>
		<header class="container-fluid">
			<div class="container py-4">

				<nav class="navbar navbar-expand-lg navbar-light">
					<img src="<?php echo URL::getBase(); ?>assets/img/pedragon.png" alt="Pedragon" width="200">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link" href="<?php echo URL::getBase(); ?>onix">Onix</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo URL::getBase(); ?>prisma">Prisma</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo URL::getBase(); ?>cruze">Cruze</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo URL::getBase(); ?>equinox">Equinox</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo URL::getBase(); ?>s10">S10</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo URL::getBase(); ?>trailblazer">Trailblazer</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>

		</header>

		<?php
			$modulo = Url::getURL( 0 );

			if( $modulo == null )
			    $modulo = "onix";

			if( file_exists( "modulos/" . $modulo . ".php" ) )
				require "modulos/" . $modulo . ".php";
			else
				require "modulos/404.php";
		?>
			<footer class="mt-5">
				<div class="container py-5">
					<div class="row">
						<div class="col-sm-4">
							<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/pedragon.png" alt="Pedragon" width="200">
							<div class="py-4"></div>
							<h3>(92) 3652-4500</h3>
							<div class="py-2"></div>
							<p>Av.Torquato Tapajos,1409 Bairro Da Paz
								<br>Manaus, AM</p>
						</div>
						<div class="col-sm-8">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.120459981285!2d-60.02728532453183!3d-3.062447619603852!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x832045d2af9c24d4!2sConcession%C3%A1ria+Chevrolet+Pedragon!5e0!3m2!1spt-BR!2sbr!4v1551298801801" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="container copyright pt-3 pb-2 text-center">
					<p>© Copyright | Todos os direitos reservados | <a href="http://vanguardacomunicacao.com.br" target="_blank">Vanguarda Comunicação</a></p>
				</div>
			</footer>


			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
			<script src="assets/js/lightbox-plus-jquery.min.js"></script>
			<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/4af7f066-b56b-4450-84a0-d2de197fb2c0-loader.js" ></script>

		</body>

	</html>