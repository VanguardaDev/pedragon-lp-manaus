	<section class="container text-center mb-5 menu-modelo">
		<div class="row">
			<a href="<?php echo URL::getBase(); ?>onix" class="col-md-4 mb-4">
				<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/onix/onix.jpg" alt="Onix">
				<h5>Onix</h5>
			</a>
			<a href="<?php echo URL::getBase(); ?>prisma" class="col-md-4 mb-4">
				<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/prisma/prisma.jpg" alt="Prisma">
				<h5>Prisma</h5>
			</a>
			<a href="<?php echo URL::getBase(); ?>cruze" class="col-md-4 mb-4">
				<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/cruze/cruze.jpg" alt="Cruze">
				<h5>Cruze</h5>
			</a>
			<a href="<?php echo URL::getBase(); ?>equinox" class="col-md-4 mb-4">
				<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/equinox/equinox.jpg" alt="Equinox">
				<h5>Equinox</h5>
			</a>
			<a href="<?php echo URL::getBase(); ?>s10" class="col-md-4 mb-4">
				<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/s10/S10.jpg" alt="S10">
				<h5>S10</h5>
			</a>
			<a href="<?php echo URL::getBase(); ?>trailblazer" class="col-md-4 mb-4">
				<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/trailblazer/trailblaze.jpg" alt="Trailblazer">
				<h5>Trailblazer</h5>
			</a>
		</div>
	</section>