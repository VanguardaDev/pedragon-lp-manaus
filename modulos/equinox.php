<div class="slide equinox">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="col-lg-5 txt-slide">
					<h1>Equinox.<br>Explore possibilidades incríveis</h1>

					<h5>Um SUV coo você nunca viu</h5>
				</div>
			</div>
			<div class="col-lg-5 py-5">
				<div role="main" id="lp-equinox-quer-saber-mais-e6e8a72801d72fd971ba"></div>
				<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
				<script type="text/javascript">
					new RDStationForms('lp-equinox-quer-saber-mais-e6e8a72801d72fd971ba-html', 'UA-103932828-20').createForm();
				</script>
			</div>
		</div>
	</div>
</div>

<section class="container py-5">
	<div class="row py-5">
		<div class="col-md-4 py-5 mt-2">
			<p style="font-size:24px;">Curta cada momento da viagem com um motor 2.0 turbo, teto solar panorâmico, assistente Easy Park e muito mais. O Chevrolet Equinox é surpreendente em todas os detalhes!</p>
		</div>
		<div class="col-md-8 text-center">
			<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/equinox/equinox-branca.jpg" alt="Equinox">
		</div>
	</div>
</section>

<section class="container-fluid">
	<div class="row">
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-1" href="<?php echo URL::getBase(); ?>assets/img/equinox/car1.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/equinox/car1.jpg" alt="Equinox"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-2" href="<?php echo URL::getBase(); ?>assets/img/equinox/car2.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/equinox/car2.jpg" alt="Equinox"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-3" href="<?php echo URL::getBase(); ?>assets/img/equinox/car3.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/equinox/car3.jpg" alt="Equinox"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-4" href="<?php echo URL::getBase(); ?>assets/img/equinox/car4.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/equinox/car4.jpg" alt="Equinox"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-5" href="<?php echo URL::getBase(); ?>assets/img/equinox/car5.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/equinox/car5.jpg" alt="Equinox"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-6" href="<?php echo URL::getBase(); ?>assets/img/equinox/car6.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/equinox/car6.jpg" alt="Equinox"></a>
			</div>
		</article>

	</div>
</section>
<section class="text-center py-5 my-5">
	<h1><strong>Veja nossos outros modelos</strong></h1>
	<h1 style="font-weight: 100;">Saia na frente com a Chevrolet!</h1>
</section>


<?php include 'modelos.php' ?>