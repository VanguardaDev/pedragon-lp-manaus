<div class="slide onix">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="col-lg-6 txt-slide">
					<h1>Onix.<br>O carro mais vendido do Brasil.</h1>

					<h5>Agora também na sua garagem</h5>
				</div>
			</div>
			<div class="col-lg-5 py-5">
				<div role="main" id="lp-onix-quer-saber-mais-69f42058deb149b77c07"></div>
				<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
				<script type="text/javascript">
					new RDStationForms('lp-onix-quer-saber-mais-69f42058deb149b77c07-html', 'UA-103932828-20').createForm();
				</script>
			</div>
		</div>
	</div>
</div>

<section class="container py-5">
	<div class="row py-5">
		<div class="col-md-4 py-5 mt-2">
			<p style="font-size:24px;">Tecnologia de conectividade, design moderno e recursos que vão tornar a viagem a sua melhor experiência nas pistas. O Onix vai surpreender você!</p>
		</div>
		<div class="col-md-8 text-center">
			<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/onix/onix-preto.jpg" alt="Onix">
		</div>
	</div>
</section>

<section class="container-fluid">
	<div class="row">
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-1" href="<?php echo URL::getBase(); ?>assets/img/onix/car1.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/onix/car1.jpg" alt="Onix"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-2" href="<?php echo URL::getBase(); ?>assets/img/onix/car2.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/onix/car2.jpg" alt="Onix"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-3" href="<?php echo URL::getBase(); ?>assets/img/onix/car3.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/onix/car3.jpg" alt="Onix"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-4" href="<?php echo URL::getBase(); ?>assets/img/onix/car4.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/onix/car4.jpg" alt="Onix"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-5" href="<?php echo URL::getBase(); ?>assets/img/onix/car5.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/onix/car5.jpg" alt="Onix"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-6" href="<?php echo URL::getBase(); ?>assets/img/onix/car6.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/onix/car6.jpg" alt="Onix"></a>
			</div>
		</article>

	</div>
</section>
<section class="text-center py-5 my-5">
	<h1><strong>Veja nossos outros modelos</strong></h1>
	<h1 style="font-weight: 100;">Saia na frente com a Chevrolet!</h1>
</section>


<?php include 'modelos.php' ?>