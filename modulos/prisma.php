<div class="slide prisma">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="col-lg-6 txt-slide">
					<h1>Prisma.<br>Mais poder nas pistas.</h1>

					<h5>O seu dia a dia muito mais fácil</h5>
				</div>
			</div>
			<div class="col-lg-5 py-5">
				<div role="main" id="lp-prisma-quer-saber-mais-f17a9cbe2052a4c3d656"></div>
				<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
				<script type="text/javascript">
					new RDStationForms('lp-prisma-quer-saber-mais-f17a9cbe2052a4c3d656-html', 'UA-103932828-20').createForm();
				</script>
			</div>
		</div>
	</div>
</div>

<section class="container py-5">
	<div class="row py-5">
		<div class="col-md-4 py-5 mt-2">
			<p style="font-size:24px;">Supertecnológico e completo para otimizar a sua rotina. O seu Chevrolet vem equipado com OnStar, Chevrolet MyLink, computador de bordo e muito mais para conquistar você.</p>
		</div>
		<div class="col-md-8 text-center">
			<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/prisma/prisma-prata.jpg" alt="Prisma">
		</div>
	</div>
</section>

<section class="container-fluid">
	<div class="row">
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-1" href="<?php echo URL::getBase(); ?>assets/img/prisma/car1.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/prisma/car1.jpg" alt="Prisma"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-2" href="<?php echo URL::getBase(); ?>assets/img/prisma/car2.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/prisma/car2.jpg" alt="Prisma"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-3" href="<?php echo URL::getBase(); ?>assets/img/prisma/car3.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/prisma/car3.jpg" alt="Prisma"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-4" href="<?php echo URL::getBase(); ?>assets/img/prisma/car4.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/prisma/car4.jpg" alt="Prisma"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-5" href="<?php echo URL::getBase(); ?>assets/img/prisma/car5.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/prisma/car5.jpg" alt="Prisma"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-6" href="<?php echo URL::getBase(); ?>assets/img/prisma/car6.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/prisma/car6.jpg" alt="Prisma"></a>
			</div>
		</article>

	</div>
</section>
<section class="text-center py-5 my-5">
	<h1><strong>Veja nossos outros modelos</strong></h1>
	<h1 style="font-weight: 100;">Saia na frente com a Chevrolet!</h1>
</section>


<?php include 'modelos.php' ?>