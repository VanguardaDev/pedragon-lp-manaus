<div class="slide s10">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="col-lg-6 txt-slide">
					<h1>S10. Força<br>para novos desafios</h1>

					<h5>Versatilidade, tecnologia e sofisticação</h5>
				</div>
			</div>
			<div class="col-lg-5 py-5">
				<div role="main" id="lp-s10-quer-saber-mais-abad3562cd88eb323d94"></div>
				<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
				<script type="text/javascript">
					new RDStationForms('lp-s10-quer-saber-mais-abad3562cd88eb323d94-html', 'UA-103932828-20').createForm();
				</script>
			</div>
		</div>
	</div>
</div>

<section class="container py-5">
	<div class="row py-5">
		<div class="col-md-4 py-5 mt-2">
			<p style="font-size:24px;">Explore as estradas e as ruas da cidade sem abrir mão do conforto. Chegou a hora de evoluir com a picape que une força e robustez.</p>
		</div>
		<div class="col-md-8 text-center">
			<img class="img-fluid" src="<?php echo URL::getBase(); ?>assets/img/s10/s10-branco.jpg" alt="S10">
		</div>
	</div>
</section>

<section class="container-fluid">
	<div class="row">
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-1" href="<?php echo URL::getBase(); ?>assets/img/s10/car1.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/s10/car1.jpg" alt="S10"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-2" href="<?php echo URL::getBase(); ?>assets/img/s10/car2.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/s10/car2.jpg" alt="S10"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-3" href="<?php echo URL::getBase(); ?>assets/img/s10/car3.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/s10/car3.jpg" alt="S10"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-4" href="<?php echo URL::getBase(); ?>assets/img/s10/car4.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/s10/car4.jpg" alt="S10"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-5" href="<?php echo URL::getBase(); ?>assets/img/s10/car5.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/s10/car5.jpg" alt="S10"></a>
			</div>
		</article>
		<article class="col-md-4">
			<div class="row">
				<a id="gallery" data-lightbox="gallery-6" href="<?php echo URL::getBase(); ?>assets/img/s10/car6.jpg"><img class="img-fluid img-gallery" src="<?php echo URL::getBase(); ?>assets/img/s10/car6.jpg" alt="S10"></a>
			</div>
		</article>

	</div>
</section>
<section class="text-center py-5 my-5">
	<h1><strong>Veja nossos outros modelos</strong></h1>
	<h1 style="font-weight: 100;">Saia na frente com a Chevrolet!</h1>
</section>


<?php include 'modelos.php' ?>